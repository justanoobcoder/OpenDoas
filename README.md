# OpenDoas: a portable version of OpenBSD's `doas` command

`doas` is a minimal replacement for the venerable `sudo`. It was
initially [written by Ted Unangst](http://www.tedunangst.com/flak/post/doas)
of the OpenBSD project to provide 95% of the features of `sudo` with a
fraction of the codebase.

Please note: This is not original. This is just a fork of
[Duncaen/OpenDoas](https://github.com/Duncaen/OpenDoas).
You can get more information about `doas` there.

## Installation

Caution: `doas` doesn't create configuration file (`/etc/doas.conf`),
so you have to create it yourself after installing `doas`; otherwise, it won't work.
Check [Configuration](#configuration) for details.

### Gentoo Linux

For Gentoo linux user, you can install this program from this
[overlay](https://github.com/justanoobcoder/gentoo-overlay).
Note: the `pam` USE flag should be enabled for this package,
enable `persist` USE flag if you want persist feature.

### Install from source code (any distros)

Step 1: Clone this repository.

    git clone https://github.com/justanoobcoder/OpenDoas.git

Step 2: Change directory to OpenDoas folder and run these following commands
to create configuration files.
Remove configure flag `--with-timestamp` if you don't want the persist feature.

    ./configure --prefix=/usr --sysconfdir=/etc --with-pam --with-timestamp

Step 3: Compile the code using `make`.

    make

Step 4: Install `doas`. Note: run this following command **as root** user.

    make install

Step 5: Create `/etc/doas.conf` file. Check [Configuration](#configuration) for details.

Step 6: Uninstall `sudo` if you have it on your system.

## Configuration

The doas configuration file is located at `/etc/doas.conf`.
To create a rule allowing a user to perform admin actions,
add this following line to `/etc/doas.conf`:

    permit your_username as root

The above line will allow `your_username` user to perform with root user permission.
For example:

    permit john as root

If you want persist feature, add `persist` option to doas.conf file.
Note: With persist feature, after the user successfully authenticates,
`doas` will not ask for a password again for 5 minutes (on the same shell session).

    permit persist john as root

If you want to give all users in a group permission to perform actions as root user.
Put a colon (`:`) before group name. For example:

    permit persist :wheel as root

If you want to allow a user (or a group) to perform a command as root without asking
for password, add a line like this:

    permit nopass john cmd touch

Now you can run `doas touch filename` without password.

Check the man page of doas.conf for more information: `man doas.conf`  
You can also take a look at my personal
[doas.conf](https://github.com/justanoobcoder/gentoo-stuff/blob/master/etc/doas.conf).


## How to use

Just like `sudo`, add `doas` in front of the command to access root permission.

    doas vim /etc/doas.conf

Check the man page of doas for more information: `man doas`
